NAME=metadataservice
VERSION=1.0.0

.PHONY: build
build:
	@go build -o bin/$(NAME)

.PHONY: run
run: build
	@./bin/$(NAME) -e development

.PHONY: run-prod
run-prod: build
	@./bin/$(NAME) -e production

.PHONY: clean
clean:
	@rm -f bin/$(NAME)

.PHONY: install
deps-save:
	@go install

.PHONY: test
test:
	@go test -v ./tests/*